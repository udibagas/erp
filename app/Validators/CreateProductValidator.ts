import { schema, rules, CustomMessages } from '@ioc:Adonis/Core/Validator'
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class CreateProductValidator {
  constructor(protected ctx: HttpContextContract) { }

  public schema = schema.create({
    code: schema.string({}, [
      rules.maxLength(20),
      rules.unique({ table: 'products', column: 'code' })
    ]),

    name: schema.string({}, [
      rules.maxLength(255),
      rules.unique({ table: 'products', column: 'name' })
    ]),

    part_number: schema.string({}, [
      rules.maxLength(255),
      rules.unique({ table: 'products', column: 'part_number' })
    ]),

    description: schema.string.optional({}, [
      rules.maxLength(255),
      rules.unique({ table: 'products', column: 'description' })
    ]),

    qtyInStock: schema.number.optional(),

    unitPrice: schema.number.optional(),
  })

  public messages: CustomMessages = {}
}
