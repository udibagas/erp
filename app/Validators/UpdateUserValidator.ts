import { schema, rules } from '@ioc:Adonis/Core/Validator'
import CreateUserValidator from './CreateUserValidator'

export default class UpdateUserValidator extends CreateUserValidator {
  public schema = schema.create({
    name: schema.string({}, [
      rules.maxLength(255)
    ]),

    email: schema.string({}, [
      rules.maxLength(255),
      rules.email(),
      rules.unique({ table: 'users', column: 'email', whereNot: { id: this.ctx.params.id } }),
    ]),

    password: schema.string({}, [
      rules.minLength(8)
    ]),

    role: schema.string.optional()
  })
}
