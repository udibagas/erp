import { schema, rules } from '@ioc:Adonis/Core/Validator'
import CreateProductValidator from './CreateProductValidator'

export default class UpdateProductValidator extends CreateProductValidator {

  public schema = schema.create({
    code: schema.string({}, [
      rules.maxLength(20),
      rules.unique({ table: 'products', column: 'code', whereNot: { id: this.ctx.params.id } })
    ]),

    name: schema.string({}, [
      rules.maxLength(255),
      rules.unique({ table: 'products', column: 'name', whereNot: { id: this.ctx.params.id } })
    ]),

    part_number: schema.string({}, [
      rules.maxLength(255),
      rules.unique({ table: 'products', column: 'part_number', whereNot: { id: this.ctx.params.id } })
    ]),

    description: schema.string.optional({}, [
      rules.maxLength(255),
      rules.unique({ table: 'products', column: 'description' })
    ]),

    qtyInStock: schema.number.optional(),

    unitPrice: schema.number.optional(),
  })
}
