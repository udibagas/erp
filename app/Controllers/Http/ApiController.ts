import { BaseModel } from '@ioc:Adonis/Lucid/Orm';
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class ApiController {

  public model = BaseModel;

  public createValidator: any;

  public updateValidator: any;

  public async index({ request, response }: HttpContextContract) {
    let { page, perPage, orderBy, sort, filters } = request.qs()
    page = page || 1;
    perPage = perPage || 1;
    orderBy = orderBy || 'id';
    sort = sort || 'asc';
    filters = filters ? JSON.parse(filters) : {}

    const query = this.model
      .query()
      .orderBy(orderBy, sort)

    if (typeof filters == 'object') {
      for (let column in filters) {
        query.orWhere(column, 'ilike', `%${filters[column]}%`)
      }
    }

    const data = await query.paginate(page, perPage)
    return response.json(data)
  }

  public async store({ request, response } : HttpContextContract) {
    const payload = await request.validate(this.createValidator)
    const data = await this.model.create(payload)
    return response.status(201).json({ message: 'Data has been created', data })
  }

  public async show({ params, response } : HttpContextContract) {
    const instance = await this.model.findOrFail(params.id)
    return response.json(instance)
  }

  public async update({ params, request, response }: HttpContextContract) {
    const data = await this.model.findOrFail(params.id)
    const payload = await request.validate(this.updateValidator)
    await data.merge(payload).save()
    return response.json({ message: 'Data has been updated', data })
  }

  public async destroy({ params, response }: HttpContextContract) {
    const instance = await this.model.findOrFail(params.id)
    await instance.delete()
    return response.json({ message: 'Data has been deleted' })
  }
}
