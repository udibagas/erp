import Product from "App/Models/Product";
import ApiController from "./ApiController";
import CreateProductValidator from "App/Validators/CreateProductValidator";
import UpdateProductValidator from "App/Validators/UpdateProductValidator";

export default class ProductsController extends ApiController {

  public model = Product;

  public createValidator = CreateProductValidator;

  public updateValidator = UpdateProductValidator;
}
