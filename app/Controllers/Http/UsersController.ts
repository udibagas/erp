import User from "App/Models/User"
import CreateUserValidator from 'App/Validators/CreateUserValidator'
import ApiController from './ApiController'
import UpdateUserValidator from "App/Validators/UpdateUserValidator";

export default class UsersController extends ApiController {

  public model = User;

  public createValidator = CreateUserValidator;

  public updateValidator = UpdateUserValidator;

}
